import Head from "next/head";
import type { NextPage } from "next";
import { createGlobalStyle } from "styled-components";

import Hero from "components/home/Hero";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
  }
`;

const Home: NextPage = () => (
  <div>
    <Head>
      <title>JobNEST - Land your dream job effortlessly!</title>
    </Head>
    <Hero />

    <GlobalStyle />
  </div>
);

export default Home;
