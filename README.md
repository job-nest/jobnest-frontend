# JobNEST - Frontend

JobNEST Frontend service

### Development
In order to run application in development profile execute:

```sh
$ yarn dev
```

### Installation
In order to install needed packages you need to execute command 

```sh
$ yarn install
```

### Running tests
In order to run tests you need to execute command 

```sh
$ yarn test
```
