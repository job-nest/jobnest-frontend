import React from "react";

import AppLogo from "components/common/AppLogo";
import NavbarList from "components/home/NavbarList";
import { NavbarSection } from "components/home/Navbar/Navbar.styles";

const Navbar = () => (
  <NavbarSection>
    <AppLogo />
    <NavbarList />
  </NavbarSection>
);

export default Navbar;
