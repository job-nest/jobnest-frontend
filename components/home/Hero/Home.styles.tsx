import styled from "styled-components";

export const HeroSection = styled.div`
  display: block;
  height: 100vh;
  background: rgba(242, 244, 251, 255);
  margin-bottom: 40px;
  border-bottom: 1px solid #ccc;
`;
