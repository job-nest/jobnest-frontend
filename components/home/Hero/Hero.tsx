import React from "react";

import Navbar from "components/home/Navbar";

import { HeroSection } from "components/home/Hero/Home.styles";

const Hero = () => {
  return (
    <HeroSection>
      <Navbar />
    </HeroSection>
  );
};

export default Hero;
