import React from "react";

import appLogo from "assets/logo/logo_color.svg";
import { AppLogoImage } from "components/common/AppLogo/AppLogo.styles";

const AppLogo = () => <AppLogoImage alt="app-logo" src={appLogo} />;

export default AppLogo;
