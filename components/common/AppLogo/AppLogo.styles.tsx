import Image from "next/image";
import styled from "styled-components";

export const AppLogoImage = styled(Image)`
  display: block;
  height: 30px !important;
`;
