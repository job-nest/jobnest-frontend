import React from "react";

const Button: React.FC<Props> = ({ text }: Props) => {
  return <div>{text}</div>;
};

interface Props {
  text: string;
}

export default Button;
